# Dump whole config
#docker exec -ti nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ config:list'" > nextcloud_config_20210308.json

# Install Social Login plugin
#su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install sociallogin'

# Redirect to mokey on login
#su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ config:system:set social_login_auto_redirect true'

# Dump Social Login config
#su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ config:list sociallogin'

# Import json with social login config
#su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ config:import sociallogin.json'

docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml"